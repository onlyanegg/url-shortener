from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def handle(self, *args, **options):
        user = get_user_model()
        if user.objects.count() == 0:
            admin = user.objects.create_superuser(
                email="admin@example.com", password="admin"
            )
            admin.save()
