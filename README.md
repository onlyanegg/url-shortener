# URL Shortener

A simple URL shortener in Django and jQuery

# Development

See [URL Shortener - Docker Compose](https://gitlab.com/onlyanegg/url-shortener-docker-compose) to set up a development environment with Docker Compose

[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy?template=https://gitlab.com/onlyanegg/url-shortener)
