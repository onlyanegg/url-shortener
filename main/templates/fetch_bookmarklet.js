(function() {
  function loadScript(url, cb) {
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = url;

    script.onreadystatechange = cb;
    script.onload = cb;

    document.head.appendChild(script);
  }

  if (window.UrlShortenerBookmarklet !== undefined) {
    UrlShortenerBookmarklet();
  } else {
    loadScript(
      "https://cdn.jsdelivr.net/npm/node-uuid@1.4.8/uuid.min.js",
      () => {
        document.body.appendChild(
          document.createElement("script")
        ).src = `http://{{ HOST }}/api/bookmarklet/?r=${uuid.v1()}`;
      }
    );
  }
})();
