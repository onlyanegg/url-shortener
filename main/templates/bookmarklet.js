function UrlShortenerBookmarklet() {
  function loadJquery(callback) {
    var jquery_version = "3.4.0";
    loadLibrary(
      `https://ajax.googleapis.com/ajax/libs/jquery/${jquery_version}/jquery.min.js`,
      window.jQuery,
      callback
    );
  }

  function loadClipboardJs(callback) {
    loadLibrary(
      "https://cdn.jsdelivr.net/npm/clipboard@2/dist/clipboard.min.js",
      window.ClipboardJS,
      callback
    );
  }

  function loadLibrary(source, testObject, callback) {
    if (typeof testObject != "undefined") {
      callback();
      return;
    }

    var script = document.createElement("script");
    script.src = source;
    document.head.appendChild(script);

    var attempts = 15;
    (function() {
      if (typeof testObject != "undefined") {
        callback();
        return;
      }

      if (--attempts <= 0) {
        alert(`An error occurred while loading ${source}`);
        return;
      }

      window.setTimeout(arguments.callee, 250);
    })();
  }

  function loadLibraries(callback) {
    // This is pretty hacky. Maybe you should use async/await instead
    var count = 0;
    function ready() {
      count += 1;
      if (count < 2) {
        return;
      }
      callback();
    }

    loadJquery(ready);
    loadClipboardJs(ready);
  }

  function bookmarkSite() {
    $.post({
      url: "http://{{ HOST }}/api/shorten/",
      data: { url: $(location).attr("href") },
      success: displayBookmark,
      failure: () => alert("Couldn't post to {{ HOST }}")
    });
  }

  function displayBookmark(data) {
    new ClipboardJS("#{{ HOST }}-pop-up");

    styles = `
      <style>
        #{{ HOST }}-pop-up {
          position: absolute;
          top: 50vh;
          left: 50vw;
          transform: translate(-50%, -50%);
          z-index: 10;
          border-radius: 5px;
          border-color: #5b618a;
          border-style: solid;
          background-color: #f4cd58;
          color: #5b618a;
          padding: 1em;
        }

        #{{ HOST }}-pop-up:hover,
        #{{ HOST }}-pop-up:active {
          background-color: #5b618a;
          color: white;
          cursor: pointer;
        }
      </style>
    `;
    popup = `
      <p id="{{ HOST }}-pop-up", data-clipboard-text="${data.url}">
        <span>Successfully shortened! </span>
        <span>${data.url}</span>
      </p>'
    `;
    $("head").append(styles);
    $("body").prepend(popup);

    $popup = $("#{{ HOST }}-pop-up");
    $popup.click(function(event) {
      event.preventDefault();

      $popup.empty();
      $popup.append("<span>Copied!</span>");

      setTimeout(function() {
        $popup.animate(
          { opacity: "0" },
          {
            complete: function() {
              $popup.remove();
            }
          }
        );
      }, 500);
    });
  }

  loadLibraries((callback = bookmarkSite));
}

UrlShortenerBookmarklet();
