"""
Utils for the URL shortener application
"""


# Built-in libs
import random
import string
from typing import TYPE_CHECKING

# Third-party libs
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.core.paginator import Paginator
from django.shortcuts import reverse as django_reverse

# Project libs
from .models import Url, User

if TYPE_CHECKING:
    from django.http import HttpRequest


def get_unique_string():
    current_strings = [
        u.unique_string for u in Url.objects.all()
    ]  # pylint: disable=no-member
    chars = string.ascii_letters + string.digits
    str_ = "".join(random.choices(chars, k=6))
    while str_ in current_strings:
        str_ = "".join(random.choices(chars, k=6))

    return str_


def reverse(
    viewname, urlconf=None, args=None, kwargs=None, current_app=None, params=None
):
    path = django_reverse(viewname)
    if params is not None:
        query_string = []
        for key, val in params.items():
            query_string.append(f"{key}={val}")

        query_string = "&".join(query_string)
        path = f"{path}?{query_string}"

    return path


class UrlPaginator(Paginator):
    def __init__(self, request, page_size, *args, **kwargs):
        if request.user.is_authenticated:
            super().__init__(
                User.objects.get(pk=request.user.pk).urls.all(),
                page_size,
                *args,
                **kwargs,
            )
        else:
            urls = SessionUrls(request)
            super().__init__(
                Url.objects.filter(pk__in=[url["pk"] for url in urls]),
                page_size,
                *args,
                **kwargs,
            )


class SessionUrls:
    def __init__(self, request: "HttpRequest") -> None:
        self._session = request.session

        if "urls" not in self._session:
            self._session["urls"] = []

    def add(self, url: "Url") -> None:
        url_dict = {}
        for attr in ["pk", "url"]:
            url_dict[attr] = getattr(url, attr)

        self._session["urls"].append(url_dict)
        self._session.modified = True

    def __iter__(self):
        for url in self._session["urls"]:
            yield url


class UserConfirmationTokenGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, user, timestamp):
        return str(user.pk) + str(timestamp) + str(user.is_active)
