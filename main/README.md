**Known Issues:**

- This is not safe for multiple instances of this application. The random
  function is within Python, so there could be a case where another
  process takes the random id while the other hasn't yet committed.
