// In landscape mode there are really two separate state machines - one for the main content, and
// another for on the authentication bar

const BASE_URL = "http://localhost";

const DEFAULT = "default";
const LOGIN_FORM = "loginForm";
const SIGNUP_FORM = "signupForm";
const URLS = "urls";

const PORTRAIT = "portrait";
const LANDSCAPE = "landscape";

const selectors = {
  authButtons: "#auth-button-container",
  loginForm: "#login-form-container",
  signupForm: "#signup-form-container",
  urlForm: "#main-container",
  urls: "#urls-container",
  spacer: "#spacer",
  drawerHandle: "#urls-button img"
};

const states = {
  portrait: {
    main: {
      default: {
        authButtons: 10,
        loginForm: 0,
        signupForm: 0,
        urlForm: 90,
        urls: 0,
        drawerHandle: "chevron-up"
      },
      loginForm: {
        authButtons: 10,
        loginForm: 30,
        signupForm: 0,
        urlForm: 60,
        urls: 0,
        drawerHandle: "chevron-up"
      },
      signupForm: {
        authButtons: 10,
        loginForm: 0,
        signupForm: 30,
        urlForm: 60,
        urls: 0,
        drawerHandle: "chevron-up"
      },
      urls: {
        authButtons: 10,
        loginForm: 0,
        signupForm: 0,
        urlForm: 30,
        urls: 60,
        drawerHandle: "chevron-down"
      }
    }
  },
  landscape: {
    // Two state machines for landscape mode - the main content, and the authBar
    main: {
      default: {
        urlForm: 100,
        urls: 0,
        drawerHandle: "chevron-up"
      },
      urls: {
        urlForm: 40,
        urls: 60,
        drawerHandle: "chevron-down"
      }
    },
    auth: {
      default: {
        loginForm: 0,
        signupForm: 0
      },
      loginForm: {
        loginForm: 30,
        signupForm: 0
      },
      signupForm: {
        loginForm: 0,
        signupForm: 30
      }
    }
  }
};

// Portrait-mode state tracker
var curState = DEFAULT;

// Landscape-mode state trackers
var authState = DEFAULT;
var mainState = DEFAULT;

$(document).ready(function() {
  new ClipboardJS(document.querySelectorAll(".copy-button, #copy-button"));

  var $showUrlsButton = $("#show-recents-button");
  var $hideUrlsButton = $("#hide-recents-button");
  var $urls = $("#recents");
  var $form = $("#form");
  var $copyButton = $("#copy-button");
  var $closeButton = $("#close-button");

  $("#urls-button").click(function(event) {
    event.preventDefault();
    if (curState == URLS) {
      goToState(states[getMode()].main.default);
      curState = DEFAULT;
    } else {
      goToState(states[getMode()].main.urls);
      curState = URLS;
    }
  });

  $("#login-button").click(function(event) {
    event.preventDefault();
    if (getMode() == PORTRAIT) {
      if (curState == SIGNUP_FORM) {
        goToState(
          states.portrait.main.default,
          goToState,
          states.portrait.main.loginForm
        );
        curState = LOGIN_FORM;
      } else if (curState == LOGIN_FORM) {
        goToState(states.portrait.main.default);
        curState = DEFAULT;
      } else {
        goToState(states.portrait.main.loginForm);
        curState = LOGIN_FORM;
      }
    } else {
      if (authState == SIGNUP_FORM) {
        goToState(
          states.landscape.auth.default,
          goToState,
          states.landscape.auth.loginForm
        );
        authState = LOGIN_FORM;
      } else if (authState == LOGIN_FORM) {
        goToState(states.landscape.auth.default);
        curState = DEFAULT;
      } else {
        goToState(states.landscape.auth.loginForm);
        authState = LOGIN_FORM;
      }
    }
  });

  $("#signup-button").click(function(event) {
    event.preventDefault();
    if (getMode() == PORTRAIT) {
      if (curState == LOGIN_FORM) {
        goToState(
          states.portrait.main.default,
          goToState,
          states.portrait.main.signupForm
        );
        curState = SIGNUP_FORM;
      } else if (curState == SIGNUP_FORM) {
        goToState(states.portrait.main.default);
        curState = DEFAULT;
      } else {
        goToState(states.portrait.main.signupForm);
        curState = SIGNUP_FORM;
      }
    } else {
      if (authState == LOGIN_FORM) {
        goToState(
          states.landscape.auth.default,
          goToState,
          states.landscape.auth.signupForm
        );
        authState = SIGNUP_FORM;
      } else if (authState == SIGNUP_FORM) {
        goToState(states.landscape.auth.default);
        authState = DEFAULT;
      } else {
        goToState(states.landscape.auth.signupForm);
        authState = SIGNUP_FORM;
      }
    }
  });

  $showUrlsButton.click(function(event) {
    event.preventDefault();

    $urls.css("display", "block");
    $urls.animate({ height: "70vh" });
    $form.animate({ height: "30vh" });

    $showUrlsButton.addClass("invisible");
    $hideUrlsButton.removeClass("invisible");
  });

  $hideUrlsButton.click(function(event) {
    event.preventDefault();

    $urls.animate(
      { height: "0" },
      {
        complete: function() {
          $urls.css("display", "none");
        }
      }
    );
    $form.animate({ height: "100vh" });

    $hideUrlsButton.addClass("invisible");
    $showUrlsButton.removeClass("invisible");
  });

  $("#next").click(function(event) {
    event.preventDefault();

    var curPage = $("#urls").data("page-number");
    $("#urls").remove();
    $.get(`${BASE_URL}/urls/?page=${parseInt(curPage) + 1}`, function(data) {
      $("#table-container").append(data);
      displayNavButtons($("#urls"), $("#next"), $("#prev"));
    });
  });

  $("#prev").click(function(event) {
    event.preventDefault();

    var curPage = $("#urls").data("page-number");
    $("#urls").remove();
    $.get(`${BASE_URL}/urls/?page=${parseInt(curPage) - 1}`, function(data) {
      $("#table-container").append(data);
      displayNavButtons($("#urls"), $("#next"), $("#prev"));
    });
  });

  $(".close-button").click(function(event) {
    event.preventDefault();
    $message = $(event.target).parents(".message-container");
    $message.remove();
  });

  $("#copy-button").click(function(event) {
    event.preventDefault();
    $messageContainer = $("#copy-button").parent();
    $messageContainer.append(
      `
      <p>Copied!</p>
      `
    );
    setTimeout(function() {
      $messageContainer.animate(
        { opacity: "0" },
        {
          complete: function() {
            $messageContainer.remove();
          }
        }
      );
    }, 500);
  });

  $(".copy-button").click(function(event) {
    event.preventDefault();

    var $eventTarget = $(event.target);
    if ($eventTarget.is(".copy-button")) {
      var $copyButton = $eventTarget;
    } else {
      var $copyButton = $eventTarget.parent();
    }

    $copyButton.append(`<p class='fading'>Copied!</p>`);
    $copiedMessage = $copyButton.children(".fading");
    setTimeout(function() {
      $copiedMessage.animate(
        { opacity: "0" },
        {
          complete: function() {
            $copiedMessage.remove();
          }
        }
      );
    }, 500);
  });

  $(".message-container").animate({ opacity: "1" });
});

function displayNavButtons($urls, $next, $previous) {
  if ($urls.data("page-number") == 1) {
    $previous.addClass("invisible");
  } else {
    $previous.removeClass("invisible");
  }

  if ($urls.data("page-number") < $urls.data("num-pages")) {
    $next.removeClass("invisible");
  } else {
    $next.addClass("invisible");
  }
}

function isHidden(element) {
  if (element.css("display") == "none") {
    return true;
  }
  return false;
}

function goToState(state, complete, ...args) {
  for (const [section, value] of Object.entries(state)) {
    if (section == "drawerHandle") {
      $(selectors[section]).attr("src", `/static/images/svg/${value}.svg`);
      continue;
    }
    animateHeightVh(section, value, complete, args[0]);
  }
}

function getMode() {
  if ($(window).width() / $(window).height() >= 5 / 4) {
    return LANDSCAPE;
  }
  return PORTRAIT;
}

function animateHeightVh(section, height, complete, ...args) {
  var $section = $(selectors[section]);
  if (height != 0) {
    $section.removeClass("invisible");

    // jQuery sets `display: none` when animating the height to 0. The following removes that
    // explicitly set style.
    $section.css("display", "");
  }
  $section.animate(
    { height: `${height}vh` },
    {
      complete: function() {
        if (height == 0) {
          $section.addClass("invisible");
        }
        if (complete !== undefined) {
          complete(args[0]);
        }
      }
    }
  );
}
