# Built-in libs
import json

# Third-party libs
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate, get_user_model, login
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.views import LoginView
from django.core.paginator import Paginator
from django.db import IntegrityError
from django.http import Http404, JsonResponse
from django.shortcuts import redirect, render, reverse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt

# Project libs
from .forms import NewUserForm, UrlForm
from .models import Url, User
from .tasks import send_confirmation_email
from .utils import (SessionUrls, UrlPaginator, UserConfirmationTokenGenerator,
                    get_unique_string)

PAGE_SIZE = 10


class IndexView(View):
    """
    Contains the main url form
    """

    def get(self, request):
        pages = UrlPaginator(request, PAGE_SIZE)
        url_form = UrlForm()
        authentication_form = AuthenticationForm()
        new_user_form = NewUserForm()

        return render(
            request,
            "index.html",
            {
                "url_form": url_form,
                "login_form": authentication_form,
                "signup_form": new_user_form,
                "pages": pages,
                "page": pages.get_page(1),
            },
        )

    def post(self, request):
        url_form = UrlForm(request.POST)
        if url_form.is_valid():
            # Check if URL has already been shortened
            try:
                url = Url.objects.get(url=url_form.cleaned_data["url"])
            except Url.DoesNotExist:
                url = url_form.save(commit=False)
                url.unique_string = get_unique_string()
                url.save()
                url_form.save_m2m()

            if request.user.is_authenticated:
                request.user.urls.add(url)
            else:
                urls = SessionUrls(request)
                urls.add(url)

            messages.info(
                request,
                f"http://{settings.HOST}/{url.unique_string}",
                extra_tags="shortened-url",
            )

            return redirect(reverse("index"))

        pages = UrlPaginator(request, PAGE_SIZE)
        authentication_form = AuthenticationForm()
        new_user_form = NewUserForm()

        return render(
            request,
            "index.html",
            {
                "url_form": url_form,
                "login_form": authentication_form,
                "signup_form": new_user_form,
                "pages": pages,
                "page": pages.get_page(1),
            },
        )


class RedirectView(View):
    """
    Redirect to the site
    """

    def get(self, request, unique_string):
        try:
            url = Url.objects.get(unique_string=unique_string)
        except:
            raise Http404
        return redirect(url.url, permanent=True)


class UrlsView(View):
    def get(self, request):
        """
        Renders an HTML fragment with a page of URLs
        """
        pages = UrlPaginator(request, PAGE_SIZE)
        page_num = request.GET.get("page", 1)
        return render(
            request, "urls.html", {"pages": pages, "page": pages.get_page(page_num)}
        )


class AuthenticationView(View):
    def post(self, request):
        """
        Authenticates a user and renders the index
        """
        authentication_form = AuthenticationForm(data=request.POST)
        if authentication_form.is_valid():
            user = authenticate(
                request,
                username=authentication_form.cleaned_data["username"],
                password=authentication_form.cleaned_data["password"],
            )
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return redirect("index")  # Add alert succesfully signed in
            authentication_form.add_error(None, "Incorrect username or password")

        pages = UrlPaginator(request, PAGE_SIZE)
        url_form = UrlForm()
        new_user_form = NewUserForm()

        return render(
            request,
            "index.html",
            {
                "url_form": url_form,
                "login_form": authentication_form,
                "signup_form": new_user_form,
                "pages": pages,
                "page": pages.get_page(1),
            },
        )


class NewUserView(View):
    def post(self, request):
        """
        Creates a new user
        """
        new_user_form = NewUserForm(request.POST)
        if new_user_form.is_valid():
            try:
                user = new_user_form.save(commit=False)
                user.is_active = False
                user.save()
            except IntegrityError:
                new_user_form.ErrorDict["email"] = (
                    "An account with this email already exists. If this is your account try "
                    "resetting your password."
                )
            else:
                for url in SessionUrls(request):
                    user.urls.add(Url.objects.get(pk=url["pk"]))
                user.save()

                send_confirmation_email.delay(user.pk)
                messages.info(
                    request,
                    (
                        "You're almost finished signing up. You should receive a confirmation email "
                        "shortly. Please follow the steps in that email to finish signing up."
                    ),
                )
                return redirect("index")

        pages = UrlPaginator(request, PAGE_SIZE)
        url_form = UrlForm()
        authentication_form = AuthenticationForm()

        return render(
            request,
            "index.html",
            {
                "url_form": url_form,
                "login_form": authentication_form,
                "signup_form": new_user_form,
                "pages": pages,
                "page": pages.get_page(1),
            },
        )


class ConfirmEmailView(View):
    def get(self, request):
        """
        Activates a new user after confirming their email
        """
        token = request.GET["token"]
        user = User.objects.get(pk=request.GET["uidb64"])
        if user is not None:
            uctg = UserConfirmationTokenGenerator()
            if uctg.check_token(user, token):
                user.is_active = True
                user.save()
                messages.info(
                    request,
                    "You have successfully completed your registration. You may now log in.",
                )
                return redirect("index")
        raise Http404


@method_decorator(csrf_exempt, name="dispatch")
class RestShortenView(View):
    def post(self, request):
        """
        Returns a JSON string containing the shortened URL. Needed for the bookmarklet.
        """
        form = UrlForm(request.POST)
        if form.is_valid():
            url = form.save(commit=False)
            url.unique_string = get_unique_string()
            url.save()
            form.save_m2m()

        data = {"url": f"http://{settings.HOST}/{url.unique_string}"}
        response = JsonResponse(data=data)
        response["access-control-allow-origin"] = "*"

        return response


class ApiBookmarkletView(View):
    def get(self, request):
        """
        Returns the bookmarklet javascript
        """

        return render(request, "bookmarklet.js")
