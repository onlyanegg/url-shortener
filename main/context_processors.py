from django.conf import settings


def common_context(request):
    return {"HOST": settings.HOST}
