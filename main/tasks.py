# Third-party libs
import logging
from time import sleep

import requests
from celery import task
from django.conf import settings
from django.core.mail import EmailMultiAlternatives

# Project libs
from .models import User
from .utils import UserConfirmationTokenGenerator, reverse

log = logging.getLogger(__name__)


@task
def send_confirmation_email(user_pk):
    user = User.objects.get(pk=user_pk)
    uctg = UserConfirmationTokenGenerator()
    link = reverse(
        "user_confirmation",
        params={"uidb64": str(user.pk), "token": uctg.make_token(user)},
    )
    text = (
        "Before you can use your account, we need to verify your email.\n"
        "Click the following link or copy and paste it into your browser to complete your "
        "registration\n"
        f"http://{settings.HOST}{link}"
    )

    resp = requests.post(
        f"https://api.mailgun.net/v3/{settings.MAILGUN_DOMAIN}/messages",
        auth=("api", settings.MAILGUN_API_KEY),
        data={
            "from": f"Onlyanegg.me <no-reply@{settings.MAILGUN_DOMAIN}>",
            "to": [user.email],
            "subject": "Verify Email",
            "text": text,
        },
    )

    return resp
