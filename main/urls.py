# Third-party libs
from django.contrib.auth.views import LogoutView
from django.urls import path

# Project libs
from .views import (ApiBookmarkletView, AuthenticationView, ConfirmEmailView,
                    IndexView, NewUserView, RedirectView, RestShortenView,
                    UrlsView)

urlpatterns = [
    path("", IndexView.as_view(), name="index"),
    path("accounts/login/", AuthenticationView.as_view(), name="login"),
    path("accounts/logout/", LogoutView.as_view(next_page="index"), name="logout"),
    path("accounts/new-user/", NewUserView.as_view(), name="new_user"),
    path(
        "accounts/confirm-email/", ConfirmEmailView.as_view(), name="user_confirmation"
    ),
    path("urls/", UrlsView.as_view(), name="urls"),
    path("api/shorten/", RestShortenView.as_view(), name="api_shorten"),
    path("api/bookmarklet/", ApiBookmarkletView.as_view(), name="api_bookmarklet"),
    path("<slug:unique_string>/", RedirectView.as_view(), name="redirect"),
]
