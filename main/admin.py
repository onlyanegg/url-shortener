# Third-party libs
from django.contrib import admin

# Project libs
from .models import Url, User


@admin.register(Url)
class UrlAdmin(admin.ModelAdmin):
    pass


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    pass
