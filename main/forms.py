# Third-party libs
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.forms.fields import CharField

# Project libs
from .models import Url, User


class UrlForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["url"].widget.attrs.update(
            {"placeholder": "https://example.com/page1/"}
        )

    class Meta:
        model = Url
        fields = ["url"]


class NewUserForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = User
        fields = ["email"]
