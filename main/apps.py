from pathlib import Path

from django.apps import AppConfig


class MainConfig(AppConfig):
    name = "main"

    def ready(self):
        """
        Tell heroku-buildpack-nginx that the application is ready to receive traffic
        https://github.com/heroku/heroku-buildpack-nginx
        """
        Path("/tmp/app-initialized").touch()
